<%@ include file="/include.jsp" %>
<%@ include file="/include-internal.jsp" %>

<jsp:useBean id="propertiesBean" type="jetbrains.buildServer.controllers.BasePropertiesBean" scope="request"/>
<jsp:useBean id="pullKeys" class="com.arcbees.pullrequest.Constants"/>

<script>
    BS.EditTriggersDialog.serializeParameters = function () {
        var e = BS.Util.serializeForm(this.formElement());
        var f = Form.getInputs(this.formElement())
            .filter(function(input) {
                return input.type === 'password' || input.hasAttribute('data-imitate-password');
            });
        if (!f) {
            return e;
        }
        for (var c = 0; c < f.length; c++) {
            var b = f[c].name;
            if (b.indexOf("prop:") != 0) {
                continue;
            }
            var a = "prop:encrypted:" + f[c].id;
            e += "&" + a + "=";
            var d = $(a).value;
            if (d == "" || f[c].value != f[c].defaultValue) {
                d = BS.Encrypt.encryptData(f[c].value, this.formElement().publicKey.value);
            }
            e += d;
        }
        return e;
    }
</script>

<%@ include file="vcsSettings.jsp" %>
<tr>
    <th>Folder Triggers:<l:star/></th>
    <td>
        <props:textProperty name="${keys.fileMatchKey}" className="longField"/>
        <span class="error" id="error_${keys.fileMatchKey}"></span>
        <span class="smallNote">List of semicolon-separated wildcards to match directories or files.</span>
    </td>
</tr>
<tr>
    <th>Branch Triggers:<l:star/></th>
    <td>
        <props:textProperty name="${keys.branchMatchKey}" className="longField"/>
        <span class="error" id="error_${keys.branchMatchKey}"></span>
        <span class="smallNote">List of semicolon-separated wildcards to match branch names.</span>
    </td>
</tr>
<l:settingsGroup title="Behaviour">
    <tr>
        <th>Approve on success:</th>
        <td>
            <props:checkboxProperty name="${pullKeys.approveOnSuccessKey}"/>
            <span class="error" id="error_${pullKeys.approveOnSuccessKey}"></span>
            <span class="smallNote">User should approve request on build success</span>
        </td>
    </tr>
</l:settingsGroup>
